using Mirror;
using UnityEngine;
using UnityEngine.InputSystem;

    [RequireComponent(typeof(CapsuleCollider))]
    [RequireComponent(typeof(CharacterController))]
    [RequireComponent(typeof(NetworkTransform))]
    [RequireComponent(typeof(Rigidbody))]

    /*[RequireComponent(typeof(InputManager))]
    [RequireComponent(typeof(Interact))]
    [RequireComponent(typeof(Movement))]
    [RequireComponent(typeof(MouseLook))]*/

public class PlayerController : NetworkBehaviour
    {
        public CharacterController characterController;
        
        void OnValidate()
        {
            if (characterController == null)
                characterController = GetComponent<CharacterController>();

            characterController.enabled = false;
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<NetworkTransform>().clientAuthority = true;
        }

        public override void OnStartLocalPlayer()
        {
            characterController.enabled = true;
            Cursor.lockState = CursorLockMode.Locked;
        }   
    }

