using UnityEngine;

public class MouseLook : MonoBehaviour
{
    [SerializeField] float sensitivityX = 8f;
    [SerializeField] float sensitivityY = 0.5f;
    [SerializeField] float globalSensivity = 1f;
    float mouseX, mouseY;

    Transform playerCamera;
    [SerializeField] float xClamp = 85f;
    float xRotation = 0;
    Vector3 targetRotation = Vector3.zero;
    private void Awake()
    {
        playerCamera = Camera.main.transform;
    }
    private void Start()
    {
        sensitivityX *= globalSensivity;
        sensitivityY *= globalSensivity;
    }
    private void Update()
    {
        transform.Rotate(Vector3.up, mouseX * Time.deltaTime);
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -xClamp, xClamp);
        targetRotation = transform.eulerAngles;
        targetRotation.x = xRotation;
        playerCamera.eulerAngles = targetRotation;
    }
    public void ReceiveInput(Vector2 mouseInput)
    {
        mouseX = mouseInput.x * sensitivityX;
        mouseY = mouseInput.y * sensitivityY;
    }

}
