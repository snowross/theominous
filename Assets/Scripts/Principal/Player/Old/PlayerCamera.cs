﻿using Mirror;
using UnityEngine;
using UnityEngine.SceneManagement;

// This sets up the scene camera for the local player


    public class PlayerCamera : NetworkBehaviour
    {
        Camera mainCam;
        [SerializeField] Vector3 cameraPosition, cameraRotation;
        void Awake()
        {
            mainCam = Camera.main;
        }

        public override void OnStartLocalPlayer()
        {
            if (mainCam != null)
            {
                // configure and make camera a child of player with 3rd person offset
                mainCam.orthographic = false;
                mainCam.transform.SetParent(transform);
                mainCam.transform.localPosition = cameraPosition;
                mainCam.transform.localEulerAngles = cameraRotation;
            }
        }

        public override void OnStopLocalPlayer()
        {
            if (mainCam != null)
            {
                mainCam.transform.SetParent(null);
                SceneManager.MoveGameObjectToScene(mainCam.gameObject, SceneManager.GetActiveScene());
                mainCam.orthographic = true;
                mainCam.transform.localPosition = new Vector3(0f, 70f, 0f);
                mainCam.transform.localEulerAngles = new Vector3(90f, 0f, 0f);
            }
        }
    }

