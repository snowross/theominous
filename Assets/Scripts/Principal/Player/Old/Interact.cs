using UnityEngine;
using UnityEngine.InputSystem;

public class Interact : MonoBehaviour
{
    RaycastHit hit;
    Ray ray;
    Mouse mouse;
    Camera mainCamera;
    private void Awake()
    {
        mouse = Mouse.current;
    }
    void OnValidate()
    {
        if (Camera.main == null)
            mainCamera = Camera.main;   
    }

    [SerializeField] float distanceToInteract = 2f;

    
    public void OnInteractPressed()
    {
        ray = mainCamera.ScreenPointToRay(mouse.position.ReadValue());
        if (Physics.Raycast(ray, out hit, distanceToInteract))
        {
               
        }
    }

}

