using UnityEngine;

public class InputManager : MonoBehaviour
{
    public Movement movement;
    public MouseLook mouseLook;
    public Interact interact;
    PlayerControls controls;
    PlayerControls.PlayerActions player;

    Vector2 horizontalInput;
    Vector2 mouseInput;

    private void OnValidate()
    {
        if (movement == null) movement = GetComponent<Movement>();
        if (mouseLook == null) mouseLook = GetComponent<MouseLook>();
        if (interact == null) interact = GetComponent<Interact>();
    }
    private void Awake()
    {
        controls = new PlayerControls();
        player = controls.Player;
       // player.Movement.performed += ctx => horizontalInput = -ctx.ReadValue<Vector2>();
       // player.Movement.canceled += ctx => horizontalInput = Vector2.zero;
       player.Interact.performed += _ => interact.OnInteractPressed();
       // player.Look.performed += ctx => mouseInput = ctx.ReadValue<Vector2>();
    }

    private void Update()
    {
        if (movement != null) movement.ReceiveInput(-player.Movement.ReadValue<Vector2>());
        if (mouseLook != null) mouseLook.ReceiveInput(player.Look.ReadValue<Vector2>());
    }

    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDestroy()
    {
        controls.Disable();
    }
}
