using Mirror;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class Movement : NetworkBehaviour
{
#pragma warning disable 649
    [Header("Movement Settings")]
    [SerializeField] float speed = 6f;

    public CharacterController controller;
    Vector2 horizontalInput = Vector2.zero;
    Vector3 direction = Vector3.zero;
    public Animator animator;
    private void OnValidate()
    {
        if (animator == null) animator = gameObject.GetComponentInChildren<Animator>();
        if (controller == null) controller = GetComponent<CharacterController>();
    }


    private void FixedUpdate()
    {

        if (!isLocalPlayer || controller == null || !controller.enabled)
            return;
        Debug.Log("movement");
        direction = (transform.right * -horizontalInput.x + transform.forward * -horizontalInput.y) * speed;
        /*direction = Vector3.Normalize(direction);
        direction = transform.TransformDirection(direction);
        direction *= speed;
        Debug.Log(direction);
        controller.SimpleMove(direction);*/
        controller.Move(direction * Time.deltaTime);
        if (animator) animator.SetFloat("Speed", direction.magnitude);
    }

    public void ReceiveInput(Vector2 _horizontalInput)
    {
        horizontalInput = _horizontalInput;
    }
}
